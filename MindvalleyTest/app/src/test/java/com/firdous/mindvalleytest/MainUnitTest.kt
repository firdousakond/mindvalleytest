package com.firdous.mindvalleytest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.firdous.mindvalleytest.api.ApiInterface
import com.firdous.mindvalleytest.model.ProfileDataModel
import com.firdous.mindvalleytest.viewmodel.ProfileViewModel
import io.reactivex.Observable
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test

import org.junit.Rule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
class MainUnitTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var profileModelList : List<ProfileDataModel>

    @Mock
    private lateinit var apiInterface : ApiInterface

    @Mock
    private lateinit var profileListObserver : Observer<List<ProfileDataModel>>

    @Mock
    private lateinit var errorObserver : Observer<String>

    private lateinit var viewModel: ProfileViewModel

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxSchedulerRule()
    }

    @Before
    fun setup()
    {
        MockitoAnnotations.initMocks(this)
       viewModel = ProfileViewModel()
    }

    @Test
    fun `Fetch Profile Test`(){
        Mockito.`when`(apiInterface.getImages(0,10)).thenReturn(Observable.just(profileModelList))
        viewModel.fetchProfileList(0)
        apiInterface.getImages(0,10)

    }

    @Test
    fun `Profile success Test` (){
        viewModel.profileMutableList.observeForever(profileListObserver)
        viewModel.onFetchListSuccess (profileModelList)
        verify(profileListObserver).onChanged(profileModelList)
    }

    @Test
    fun `Profile Error Test` (){
        viewModel.mutableErrorMessage.observeForever(errorObserver)
        viewModel.onFetchListError ("error")
        verify(errorObserver).onChanged("error")
    }

}
