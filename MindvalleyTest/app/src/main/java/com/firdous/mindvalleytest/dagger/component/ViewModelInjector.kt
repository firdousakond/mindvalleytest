package com.firdous.mindvalleytest.dagger.component

import com.firdous.mindvalleytest.dagger.module.ApiModule
import com.firdous.mindvalleytest.viewmodel.ProfileViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApiModule::class)])
interface ViewModelInjector {

    fun inject(profileViewModel: ProfileViewModel)
}