package com.firdous.mindvalleytest.model

import com.google.gson.annotations.SerializedName

data class ProfileDataModel(
    @SerializedName("user")
    val userModel : UserModel
)
data class UserModel(
    @SerializedName("name")
    val name : String,
    @SerializedName("profile_image")
    val profileImageModel : ProfileImageModel
)
data class ProfileImageModel(
    @SerializedName("small")
    val small : String,
    @SerializedName("medium")
    val medium : String,
    @SerializedName("large")
    val large : String
)