package com.firdous.mindvalleytest.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.firdous.mindvalleytest.R
import com.firdous.mindvalleytest.adapter.ProfileAdapter
import com.firdous.mindvalleytest.model.ProfileDataModel
import com.firdous.mindvalleytest.util.AppConstant
import com.firdous.mindvalleytest.util.CommonUtils
import com.firdous.mindvalleytest.viewmodel.ProfileViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_toolbar.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ProfileViewModel
    private lateinit var profileAdapter: ProfileAdapter
    private var offset = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        initUI()
    }

    private fun initUI(){
        txt_header.text = getString(R.string.profile_list)
        viewModel.profileMutableList.observe(this, Observer { updateList(it) })
        viewModel.mutableErrorMessage.observe(this, Observer {
            if(it != null) showError(it)
        })
        setProfileAdapter()

    }

    private fun setProfileAdapter() {
        profileAdapter = ProfileAdapter(this)
        rv_images.adapter = profileAdapter
        val layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        rv_images.layoutManager = layoutManager

        if(!CommonUtils.isNetworkAvailable(this)){
            CommonUtils.displaySnackbar(this,getString(R.string.msg_no_internet), AppConstant.MESSAGE_TYPE_ERROR)
            return
        }
        rv_images.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == SCROLL_STATE_IDLE
                    && layoutManager.findLastCompletelyVisibleItemPosition()>= profileAdapter.itemCount -1
                    && profileAdapter.itemCount <= (layoutManager.findLastVisibleItemPosition() + AppConstant.LIMIT)) {
                    offset++
                    viewModel.fetchProfileList(offset)
                }
            }
        })
        viewModel.fetchProfileList(offset)
    }

    private fun updateList(profileList : List<ProfileDataModel>) {
        if(profileList.isEmpty()){
            txt_no_data.visibility = View.VISIBLE
        }
        else {
            txt_no_data.visibility = View.GONE
            profileAdapter.updateImageList(profileList)
        }
    }

    private fun showError(errorMessage : String){
        CommonUtils.displaySnackbar(this,errorMessage, AppConstant.MESSAGE_TYPE_ERROR)
    }
}
