package com.firdous.mindvalleytest.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.firdous.mindvalleytest.R
import com.firdous.mindvalleytest.util.AppConstant
import com.firdous.mindvalleytest.util.ImageUtils
import kotlinx.android.synthetic.main.activity_full_image.*
import kotlinx.android.synthetic.main.activity_toolbar.*

class FullImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_image)
        initView()
        setListener()
        getBundleData()
    }

    private fun initView() {
        txt_header.text = getString(R.string.desc_profile_image)
        ibtn_back.visibility = View.VISIBLE
    }

    private fun setListener() {
        ibtn_back.setOnClickListener { finish() }
    }

    private fun getBundleData() {
        if (intent != null) {
            val imageUrl = intent.getStringExtra(AppConstant.IMAGE_URL)
            if(imageUrl != null) {
                ImageUtils.loadImageFromUrl(this, img_profile, imageUrl)
            }

        }
    }
}
