package com.firdous.mindvalleytest.viewmodel

import androidx.lifecycle.MutableLiveData
import com.firdous.mindvalleytest.api.ApiInterface
import com.firdous.mindvalleytest.base.BaseViewModel
import com.firdous.mindvalleytest.model.ProfileDataModel
import com.firdous.mindvalleytest.util.AppConstant
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProfileViewModel : BaseViewModel() {

    val profileMutableList : MutableLiveData<List<ProfileDataModel>> = MutableLiveData()
    val mutableErrorMessage : MutableLiveData<String> = MutableLiveData()

    @Inject
    lateinit var apiInterface: ApiInterface
    lateinit var compositeDisposable: Disposable

    fun fetchProfileList(offset : Int ){

        compositeDisposable = apiInterface.getImages(offset, AppConstant.LIMIT)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> onFetchListSuccess(result) },
                { it.message?.let { it1 -> onFetchListError(it1) } })
    }

    fun onFetchListSuccess(postResult: List<ProfileDataModel>) {
        profileMutableList.value = postResult
    }

    fun onFetchListError(error : String){
        mutableErrorMessage.value = error
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}