package com.firdous.mindvalleytest.util

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.firdous.mindvalleytest.R

class ImageUtils  {

    companion object{
        /**
         * Load IMAGE from url.
         *
         */
        fun loadImageFromUrl(context: Context, imageView: ImageView, url: String) {

                val glideUrl = GlideUrl(url)
                Glide.with(context)
                    .load(glideUrl).apply(
                        RequestOptions()
                            .timeout(AppConstant.CONSTANT_GLIDE_TIMEOUT)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .error(R.drawable.img_loading)
                            .placeholder(R.drawable.progress_animation)
                    )
                    .into(imageView)
            }

        }

}
