package com.firdous.mindvalleytest.util

interface AppConstant {

    companion object{
        const val CONSTANT_GLIDE_TIMEOUT = 120000
        const val MESSAGE_TYPE_SUCCESS = 100
        const val MESSAGE_TYPE_ERROR = 200
        const val IMAGE_URL = "IMAGE_URL"
        const val LIMIT = 10
    }
}