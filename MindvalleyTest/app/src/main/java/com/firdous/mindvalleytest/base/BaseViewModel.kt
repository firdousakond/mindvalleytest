package com.firdous.mindvalleytest.base

import androidx.lifecycle.ViewModel
import com.firdous.mindvalleytest.dagger.component.DaggerViewModelInjector
import com.firdous.mindvalleytest.dagger.component.ViewModelInjector
import com.firdous.mindvalleytest.dagger.module.ApiModule
import com.firdous.mindvalleytest.viewmodel.ProfileViewModel

abstract class BaseViewModel : ViewModel() {

    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .apiModule(ApiModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is ProfileViewModel -> injector.inject(this)
        }
    }
}