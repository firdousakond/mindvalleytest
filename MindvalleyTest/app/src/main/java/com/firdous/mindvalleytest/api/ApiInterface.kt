package com.firdous.mindvalleytest.api

import com.firdous.mindvalleytest.model.ProfileDataModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("/raw/wgkJgazE")
    fun getImages(@Query("offset") offset : Int, @Query("limit") limit : Int) : Observable<List<ProfileDataModel>>
}