package com.firdous.mindvalleytest.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.firdous.mindvalleytest.R
import com.firdous.mindvalleytest.activity.FullImageActivity
import com.firdous.mindvalleytest.model.ProfileDataModel
import com.firdous.mindvalleytest.util.AppConstant
import com.firdous.mindvalleytest.util.ImageUtils
import kotlinx.android.synthetic.main.item_images.view.*

class ProfileAdapter (var context: Context) : RecyclerView.Adapter<ProfileAdapter.ImageViewHolder>() {

    private var profileModelList : List<ProfileDataModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_images, parent, false)
        return ImageViewHolder(view)
    }

    override fun getItemCount(): Int {
        return if(profileModelList.isNotEmpty()){
            profileModelList.size
        } else
            0
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
      holder.setView(profileModelList[position])
    }

    fun updateImageList(uploadModelList:List<ProfileDataModel>){
        this.profileModelList = uploadModelList
        notifyDataSetChanged()
    }

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener { gotoFullScreenImage() }
        }
        fun setView(profileDataModel: ProfileDataModel) {
            ImageUtils.loadImageFromUrl(
                context,
                itemView.img_photo,
                profileDataModel.userModel.profileImageModel.large
            )
            itemView.txt_name.text = profileDataModel.userModel.name
        }

        private fun gotoFullScreenImage() {
            val intent = Intent(context, FullImageActivity::class.java)
            intent.putExtra(AppConstant.IMAGE_URL,profileModelList[adapterPosition].userModel.profileImageModel.medium)
            context.startActivity(intent)
        }

    }

}