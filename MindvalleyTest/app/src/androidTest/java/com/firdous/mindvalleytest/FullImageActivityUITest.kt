package com.firdous.mindvalleytest

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import com.firdous.mindvalleytest.activity.FullImageActivity
import com.firdous.mindvalleytest.activity.MainActivity
import org.junit.Rule
import org.junit.Test

class FullImageActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(FullImageActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully(){
        ActivityScenario.launch(FullImageActivity::class.java)
    }

    @Test
    fun checkImageVisible(){
        onView(withId(R.id.img_profile)).check(matches(isDisplayed()))
    }

    @Test
    fun backButtonClick(){
        onView(withId(R.id.ibtn_back)).perform(click())
        ActivityScenario.launch(MainActivity::class.java)
    }
}