package com.firdous.mindvalleytest

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.firdous.mindvalleytest.activity.FullImageActivity
import com.firdous.mindvalleytest.activity.MainActivity
import org.junit.Rule
import org.junit.Test

class MainActivityUITest {

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun activityLaunchesSuccessfully() {
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun checkRecyclerViewVisibility(){
        onView(withId(R.id.rv_images)).check(matches(isDisplayed()))
    }

   @Test
   fun checkRecyclerListEmpty(){

       onView(withId(R.id.rv_images)).check(matches(hasChildCount(0)))
       onView(withId(R.id.txt_no_data)).perform(setTextViewVisibility(true))
   }

    @Test
    fun recyclerItemClick(){
        val recyclerView : RecyclerView = activityTestRule.activity.findViewById(R.id.rv_images)
        if(recyclerView.adapter!!.itemCount > 0){
            onView(withId(R.id.rv_images)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()));
            ActivityScenario.launch(FullImageActivity::class.java)
        }
    }

    private fun setTextViewVisibility(value: Boolean): ViewAction {
        return object : ViewAction {

            override fun getConstraints(): org.hamcrest.Matcher<View> {
                return isAssignableFrom(TextView::class.java)
            }

            override fun perform(uiController: UiController, view: View) {
                view.visibility = if (value) View.VISIBLE else View.GONE
            }

            override fun getDescription(): String {
                return "Show / Hide View"
            }
        }
    }

}